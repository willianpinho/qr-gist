//
//  History.swift
//  QR-Gist
//
//  Created by Willian Pinho on 16/03/18.
//  Copyright © 2018 Willian Pinho. All rights reserved.
//

import Foundation
import ObjectMapper

class History: Mappable {
    var user: User?
    var version: String?
    var commitedAt: Date?
    var changeStatus: ChangeStatus?
    var url: String?
    
    required init?(map: Map) {
        
    }
    // Mappable
    func mapping(map: Map) {
        user <- map["user"]
        version <- map["version"]
        commitedAt <- map["commited_at"]
        changeStatus <- map["change_status"]
        url <- map["url"]
    }
    
}
