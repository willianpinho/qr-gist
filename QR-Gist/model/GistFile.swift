//
//  GistFile.swift
//  QR-Gist
//
//  Created by Willian Pinho on 16/03/18.
//  Copyright © 2018 Willian Pinho. All rights reserved.
//

import Foundation
import ObjectMapper

class GistFile : Mappable {
    
    var filename: String?
    var type: String?
    var language: String?
    var rawUrl: String?
    var size: Int?
    var truncated: Bool?
    var content: String?
    
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        filename <- map["filename"]
        type <- map["type"]
        language <- map["language"]
        rawUrl <- map["raw_url"]
        size <- map["size"]
        truncated <- map["bool"]
        content <- map["content"]
    }
    
}
