//
//  Gist.swift
//  QR-Gist
//
//  Created by Willian Pinho on 16/03/18.
//  Copyright © 2018 Willian Pinho. All rights reserved.
//

import Foundation
import ObjectMapper

class Gist: Mappable {
    
    var url: String?
    var forksUrl: String?
    var commitsUrl: String?
    var id: String?
    var gitPullUrl: String?
    var gitPushUrl: String?
    var htmlUrl: String?
    var files: [GistFile]?
    var isPublic:Bool?
    var createdAt: Date?
    var updatedAt: Date?
    var description: String?
    var comments: Int?
    var user: User?
    var commentsUrl: String?
    var owner: User?
    var forks: String?
    var history: String?
    var truncated: Bool?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        url <- map["url"]
        forksUrl <- map["forks_url"]
        commitsUrl <- map["commits_url"]
        id <- map["id"]
        gitPullUrl <- map["git_pull_url"]
        gitPushUrl <- map["git_push_url"]
        htmlUrl <- map["html_url"]
        files <- map["files"]
        isPublic <- map["public"]
        createdAt <- map["created_at"]
        updatedAt <- map["updated_at"]
        description <- map["description"]
        comments <- map["comments"]
        user <- map["user"]
        commentsUrl <- map["comments_url"]
        owner <- map["owner"]
        forks <- map["forks"]
        history <- map["history"]
        truncated <- map["truncated"]
    }

}
