//
//  ChangeStatus.swift
//  QR-Gist
//
//  Created by Willian Pinho on 16/03/18.
//  Copyright © 2018 Willian Pinho. All rights reserved.
//


import Foundation
import ObjectMapper

class ChangeStatus: Mappable {
    var total: Int?
    var additions: Int?
    var deletions: Int?
    
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        total <- map["total"]
        additions <- map["additions"]
        deletions <- map["deletions"]
    }
    
}
