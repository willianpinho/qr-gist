//
//  Comment.swift
//  QR-Gist
//
//  Created by Willian Pinho on 16/03/18.
//  Copyright © 2018 Willian Pinho. All rights reserved.
//

import Foundation
import ObjectMapper

class Comment: Mappable {
    var url: String?
    var id: Int?
    var user: User?
    var authorAssociation: String?
    var createdAt: Date?
    var updatedAt: Date?
    var body: String?
    
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        url <- map["url"]
        id <- map["id"]
        user <- map["user"]
        authorAssociation <- map["author_association"]
        createdAt <- map["created_at"]
        updatedAt <- map["updated_at"]
        body <- map["body"]        
    }
    
}

