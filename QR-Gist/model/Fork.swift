//
//  Fork.swift
//  QR-Gist
//
//  Created by Willian Pinho on 16/03/18.
//  Copyright © 2018 Willian Pinho. All rights reserved.
//

import Foundation
import ObjectMapper

class Fork: Mappable {
    var url: String?
    var user: User?
    var id: String?
    var createdAt: Date?
    var updatedAt: Date?
    
    required init?(map: Map) {
        
    }
    // Mappable
    func mapping(map: Map) {
        url <- map["url"]
        user <- map["user"]
        id <- map["id"]
        createdAt <- map["created_at"]
        updatedAt <- map["updated_at"]
        
    }
    
}
