//
//  Alert.swift
//  QR-Gist
//
//  Created by Willian Pinho on 16/03/18.
//  Copyright © 2018 Willian Pinho. All rights reserved.
//

import Foundation
import UIKit

class Alert {
    static func showMessage(title: String?, message: String?) -> UIAlertController {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        return alert
    }
    
    static func addAction(title: String, alert: UIAlertController, action: UIAlertAction) {
        action.setValuesForKeys(
            ["title" : title,
             "style" :  UIAlertActionStyle.default
             ])
        alert.addAction(action)
    }
    static func addOkAction(alert: UIAlertController) -> UIAlertAction {
        let okButton = UIAlertAction(title: "Ok", style: .default) { (alertAction) in
            alert.dismiss(animated: true, completion: nil)
        }
        
        return okButton
    }
}
