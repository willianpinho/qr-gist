//
//  UITableView+Extensions.swift
//  QR-Gist
//
//  Created by Willian Pinho on 16/03/18.
//  Copyright © 2018 Willian Pinho. All rights reserved.
//

import UIKit

extension UITableView {
    
    func registerNibFrom(_ cellClass: UITableViewCell.Type) {
        let nib = UINib(nibName: cellClass.className(), bundle: nil)
        self.register(nib, forCellReuseIdentifier: cellClass.className())
    }
    
    func registerNibs(_ cellClasses : [UITableViewCell.Type]) {
        cellClasses.forEach { self.registerNibFrom($0) }
    }
}
