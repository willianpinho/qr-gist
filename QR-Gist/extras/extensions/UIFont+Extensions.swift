//
//  UIFont+Extensions.swift
//  QR-Gist
//
//  Created by Willian Pinho on 16/03/18.
//  Copyright © 2018 Willian Pinho. All rights reserved.
//

import UIKit
extension UIFont {
   
    class func openWorkSansWithType(withSize size: Int, type: WorkSansType) -> UIFont {
        guard let font = UIFont(name: "WorkSans-\(type)", size: CGFloat(size)) else {
            return UIFont(name: "WorkSans", size: CGFloat(size))!
        }
        return font
    }
}

enum WorkSansType:String {
    case black = "Black"
    case bold = "Bold"
    case extraBold = "ExtraBold"
    case extraLight = "ExtraLight"
    case light = "Light"
    case medium = "Medium"
    case regular = "Regular"
    case semiBold = "SemiBold"
    case thin = "Thin"
}
