//
//  UIColor+Extensions.swift
//  QR-Gist
//
//  Created by Willian Pinho on 16/03/18.
//  Copyright © 2018 Willian Pinho. All rights reserved.
//

import UIKit

extension UIColor {
    class var willianPinhoBlue: UIColor {
        return UIColor(red:0.200, green:0.239, blue:0.502, alpha:1.0)
    }
    
    class var willianPinhoOrange: UIColor {
        return UIColor(red:0.855, green:0.471, blue:0.251, alpha:1.0)
    }
    
    class var willianPinhoGray: UIColor {
        return UIColor(red:0.541, green:0.545, blue:0.541, alpha:1.0)
    }

}
