//
//  UIViewController+Extensions.swift
//  QR-Gist
//
//  Created by Willian Pinho on 16/03/18.
//  Copyright © 2018 Willian Pinho. All rights reserved.
//

import Foundation
import UIKit 

extension UIViewController {
    func setNavigation(title: String, withTintColor tintColor: UIColor, barTintColor: UIColor, andAttributes attributes: [NSAttributedStringKey : Any], prefersLargeTitles: Bool) {
        let navigationBar = navigationController!.navigationBar
        
        navigationBar.tintColor = tintColor
        navigationBar.titleTextAttributes = attributes
        navigationBar.barTintColor = barTintColor
        navigationBar.prefersLargeTitles = prefersLargeTitles
        navigationBar.largeTitleTextAttributes = [NSAttributedStringKey.foregroundColor: tintColor]
        self.title = title
    }
    
    func removeNavBarLine(){
        let navigationBar = navigationController!.navigationBar
        navigationBar.isTranslucent = false
        navigationBar.shadowImage = UIImage()
    }
    
    func removeBackButtonTitle() {
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: self, action: nil)
    }
    
    func openViewControllerWithIdentifier(storyBoard: String, identifier: String) {
        let storyboard = UIStoryboard(name: storyBoard, bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: identifier)
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    func openViewControllerWithIdentifierWithGistId(storyBoard: String, identifier: String, gistId: String?) {
        let storyboard = UIStoryboard(name: storyBoard, bundle: nil)
        guard let vc = storyboard.instantiateViewController(withIdentifier: identifier) as? GistViewController else {
            return print("Can't open ViewController")
        }
        
        vc.gistId = gistId
        
        self.navigationController?.pushViewController(vc, animated: false)
    }
}
