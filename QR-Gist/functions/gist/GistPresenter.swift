//
//  GistPresenter.swift
//  QR-Gist
//
//  Created by Willian Pinho on 16/03/18.
//  Copyright © 2018 Willian Pinho. All rights reserved.
//

import Foundation

protocol GistView: NSObjectProtocol {
    func setupCells(gist: Gist)
    func reloadTableView()
}

class GistPresenter: NSObject {
    var view: GistView?
    
    func setViewDelegate(view: GistView) {
        self.view = view
    }
    
    func loadGistFromServer(gistId: String, completion: @escaping (Bool?, String?, Gist?) -> Void)  {
        GistService.getGist(gistId: gistId) { (success, message, gist) in
            if success {
                completion(true, nil, gist!)
            } else {
                completion(false, message!, nil)
            }
        }
    }
    
    func loadGistCommentFromServer(gist: Gist, completion: @escaping (Bool?, String?, [Comment]?) -> Void)  {
        GistService.getCommentsFromGist(gist: gist) { (success, message, comments) in
            if success {
                completion(true, nil, comments)
            } else {
                completion(false, message!, nil)
            }
        }
    }
    
    func loadGistInformations(gist: Gist) {
        DispatchQueue.main.async {
            self.view?.setupCells(gist: gist)
            self.view?.reloadTableView()
        }
    }
}
