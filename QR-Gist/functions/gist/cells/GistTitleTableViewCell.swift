//
//  GistTitleTableViewCell.swift
//  QR-Gist
//
//  Created by Willian Pinho on 16/03/18.
//  Copyright © 2018 Willian Pinho. All rights reserved.
//

import UIKit

class GistTitleTableViewCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
}
