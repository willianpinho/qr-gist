//
//  GistViewController.swift
//  QR-Gist
//
//  Created by Willian Pinho on 16/03/18.
//  Copyright © 2018 Willian Pinho. All rights reserved.
//

import UIKit
import Kingfisher

class GistViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var presenter = GistPresenter()
    var gistId: String?
    var gist: Gist?
    var cells: [UITableViewCell] = []
    
    func setPresenterDelegate() {
        presenter.setViewDelegate(view: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setPresenterDelegate()
        self.customLayout()
        self.setupTableView(tableView: self.tableView)
        self.loadGist()
    }
    
    func loadGist() {
        guard let gistId = gistId else {
            return print("Gist não encontrado")
        }
        presenter.loadGistFromServer(gistId: gistId) { (success, message, gist) in
            if success! {
                guard let gist = gist else {
                    return print("Cant Open")
                }
                
                self.gist = gist
                self.presenter.loadGistInformations(gist: gist)
                
            } else {
                print("Cant Open")
            }
        }
    }

    func customLayout() {
        self.removeNavBarLine()
        self.setNavigation(title: gistId!, withTintColor: .willianPinhoOrange, barTintColor: .willianPinhoBlue, andAttributes: [NSAttributedStringKey.font: UIFont.openWorkSansWithType(withSize: 24, type: WorkSansType.light), NSAttributedStringKey.foregroundColor: UIColor.willianPinhoOrange], prefersLargeTitles: false)
    }
}

extension GistViewController: GistView {
    func setupCells(gist: Gist) {
        var currentCells:[UITableViewCell] = []
        
        if let cell = self.tableView.dequeueReusableCell(withIdentifier: "GistHeaderTableViewCell") as? GistHeaderTableViewCell {
            guard let urlImage = gist.owner?.avatarUrl else {
                return print("Can't open Image")
            }
            guard let login = gist.owner?.login else {
                return print("Can't get gist login")
            }
            cell.ownerNameLabel.text = login
            cell.ownerImageView.kf.setImage(with: URL(string: urlImage))
            
            currentCells.append(cell)
        }

        if let cell = self.tableView.dequeueReusableCell(withIdentifier: "GistInformationsTableViewCell") as? GistInformationsTableViewCell {
            currentCells.append(cell)
        }
        
        if let cell = self.tableView.dequeueReusableCell(withIdentifier: "GistTitleTableViewCell") as? GistTitleTableViewCell {
            guard let numberOfComments = gist.comments else {
                return print("Can't take number of comments")
            }
            cell.titleLabel.text = "Comments \(numberOfComments)"
            currentCells.append(cell)
        }

        cells = currentCells
    }
    
    func reloadTableView() {
        self.tableView.reloadData()
    }
    
    
}
extension GistViewController: UITableViewDelegate, UITableViewDataSource {
    func setupTableView(tableView: UITableView) {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.registerNibs([
            GistHeaderTableViewCell.self,
            GistCommentsTableViewCell.self,
            GistTitleTableViewCell.self,
            GistInformationsTableViewCell.self
            ])
        tableView.estimatedRowHeight = 44
        tableView.rowHeight = UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cells.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return cells[indexPath.row]
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if cells[indexPath.row] .isKind(of: GistInformationsTableViewCell.self) {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            if let vc = storyboard.instantiateViewController(withIdentifier: "GistInformationViewController") as? GistInformationViewController {
                vc.gist = self.gist!
                self.navigationController?.pushViewController(vc, animated: false)
            }
        } else if cells[indexPath.row] .isKind(of: GistTitleTableViewCell.self) {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            if let vc = storyboard.instantiateViewController(withIdentifier: "GistCommentsViewController") as? GistCommentsViewController {
                vc.gist = self.gist!
                self.navigationController?.pushViewController(vc, animated: false)
            }
        }
    }
}
