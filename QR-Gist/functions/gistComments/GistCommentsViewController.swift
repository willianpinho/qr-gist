//
//  GistCommentsViewController.swift
//  QR-Gist
//
//  Created by Willian Pinho on 16/03/18.
//  Copyright © 2018 Willian Pinho. All rights reserved.
//

import UIKit
import Kingfisher
import SafariServices

let kCloseSafariViewControllerNotification = "kCloseSafariViewControllerNotification"

class GistCommentsViewController: UIViewController, SFSafariViewControllerDelegate {

    @IBOutlet weak var connectWithGH: UIButton!
    @IBOutlet weak var commentTF: UITextField!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var commentButton: UIButton!
    
    var safariVC: SFSafariViewController?
    
    var presenter = GistCommentsPresenter()
    var gist: Gist?
    var cells: [UITableViewCell] = []
    
    func setPresenterDelegate() {
        presenter.setViewDelegate(view: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setPresenterDelegate()
        self.customLayout()
        self.setupTableView(tableView: self.tableView)
        self.loadComments()
        NotificationCenter.default.addObserver(self, selector: #selector(safariLogin(notification:)), name: NSNotification.Name(rawValue: kCloseSafariViewControllerNotification), object: nil)

    }
    
    func customLayout() {
        self.removeNavBarLine()
        self.setNavigation(title: "Comments", withTintColor: .willianPinhoOrange, barTintColor: .willianPinhoBlue, andAttributes: [NSAttributedStringKey.font: UIFont.openWorkSansWithType(withSize: 20, type: WorkSansType.light), NSAttributedStringKey.foregroundColor: UIColor.willianPinhoOrange], prefersLargeTitles: false)
        self.connectWithGH.addTarget(self, action: #selector(connectWithGithub), for: UIControlEvents.touchUpInside)
        self.commentButton.isHidden = true
        self.commentTF.isHidden = true
    }
    
    func loadComments() {
        guard let gist = gist else {
            return print("Gist não encontrado")
        }
        presenter.loadGistCommentFromServer(gist: gist) { (success, message, comments) in
            if success! {
                if let comments = comments {
                    self.presenter.loadGistComments(comments: comments)
                }
            } else {
                print("Cant Open")
            }
        }
    }
    
    @objc func connectWithGithub() {
        let authURL = URL(string: "https://github.com/login/oauth/authorize") // the URL goes here
        safariVC = SFSafariViewController(url: authURL!)
        safariVC!.delegate = self
        self.present(safariVC!, animated: true, completion: nil)
    }
    
    func safariViewControllerDidFinish(_ controller: SFSafariViewController) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    @objc func safariLogin(notification: NSNotification) {
        // get the url form the auth callback
        let url = notification.object as! NSURL
        // then do whatever you like, for example :
        // get the code (token) from the URL
        // and do a request to get the information you need (id, name, ...)
        // Finally dismiss the Safari View Controller with:
        self.safariVC!.dismiss(animated: true, completion: nil)
    }
}

extension GistCommentsViewController: GistCommentsView {
    func setupComments(comments: [Comment]) {
        var currentCells:[UITableViewCell] = []
        
        for c in comments {
            if let cell = self.tableView.dequeueReusableCell(withIdentifier: "GistCommentTableViewCell") as? GistCommentTableViewCell {
                cell.commentLabel.text = c.body
                guard let urlImage = c.user?.avatarUrl else {
                    return print("Can't Take Image Url")
                }
                cell.userImageView.kf.setImage(with: URL(string: urlImage))

                currentCells.append(cell)
            }
        }
        
        cells = currentCells
    }
    
    func reloadTableView() {
        self.tableView.reloadData()
    }
}

extension GistCommentsViewController: UITableViewDataSource, UITableViewDelegate {
    func setupTableView(tableView: UITableView) {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.registerNibs([
            GistCommentTableViewCell.self,
            ])
        tableView.estimatedRowHeight = 44
        tableView.rowHeight = UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cells.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return cells[indexPath.row]
    }
}
