//
//  GistCommentsPresenter.swift
//  QR-Gist
//
//  Created by Willian Pinho on 16/03/18.
//  Copyright © 2018 Willian Pinho. All rights reserved.
//

import Foundation

protocol GistCommentsView: NSObjectProtocol {
    func setupComments(comments: [Comment])
    func reloadTableView()
}

class GistCommentsPresenter: NSObject {
    var view: GistCommentsView?
    
    func setViewDelegate(view: GistCommentsView) {
        self.view = view
    }
    
    func loadGistCommentFromServer(gist: Gist, completion: @escaping (Bool?, String?, [Comment]?) -> Void)  {
        GistService.getCommentsFromGist(gist: gist) { (success, message, comments) in
            if success {
                completion(true, nil, comments)
            } else {
                completion(false, message!, nil)
            }
        }
    }
    
    func loadGistComments(comments: [Comment]) {
        DispatchQueue.main.async {
            self.view?.setupComments(comments: comments)
            self.view?.reloadTableView()
        }
    }
    
    func loginUser(completion: @escaping (Bool?, String?, User?) -> Void) {
        UserService.loginOAuthAuthorizer { (success, message, user) in
            if success {
                completion(true, nil, user)
            } else {
                completion(false, message!, nil)
            }
        }
    }
}
