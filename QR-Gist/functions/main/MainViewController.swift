//
//  ViewController.swift
//  QR-Gist
//
//  Created by Willian Pinho on 16/03/18.
//  Copyright © 2018 Willian Pinho. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {
    
    @IBOutlet weak var qrImageView: UIImageView!
    @IBOutlet weak var scanGistButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        definesPresentationContext = true

        self.customLayout()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }
    
    func customLayout() {
        addGestureImage(image: qrImageView, selector: #selector(openLink))
        scanGistButton.addTarget(self, action: #selector(goToScan), for: UIControlEvents.touchUpInside)
    }
    
    
    
    func addGestureImage(image: UIImageView, selector: Selector) {
        let longPressGestureRecognizer = UILongPressGestureRecognizer(target: self, action: selector)
        image.isUserInteractionEnabled = true
        image.addGestureRecognizer(longPressGestureRecognizer)
    }
    
    @objc func openLink() {
        let actionSheet = UIAlertController(title: "I'm also very curious ", message: nil, preferredStyle: .actionSheet)
        actionSheet.addAction(UIAlertAction(title: "Open My LinkedIn Profile", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            guard let url = URL(string: "linkedin://profile/willianpinho") else {
                return print("Can't Open Link ")
            }
            
            self.openUrl(url: url)
        }))
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        present(actionSheet, animated: true, completion: nil)
    }
    
    func openUrlWithSafari() {
        guard let url = URL(string: "https://www.linkedin.com/in/willianpinho/") else {
            return print("Erro ao abrir link")
        }
        self.openUrl(url: url)
    }
    
    
    func openUrl(url:URL) {
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: { (success) in
                if !success {
                    self.openUrlWithSafari()
                }
            })
        } else {
            UIApplication.shared.open(url, options: [:], completionHandler: { (success) in
                if !success {
                    self.openUrlWithSafari()
                }
            })
        }
    }
    
    @objc func goToScan() {
        self.openViewControllerWithIdentifier(storyBoard: "Main", identifier: "ScanViewController")
    }
}

