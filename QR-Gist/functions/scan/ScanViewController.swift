//
//  ScanViewController.swift
//  QR-Gist
//
//  Created by Willian Pinho on 16/03/18.
//  Copyright © 2018 Willian Pinho. All rights reserved.
//

import UIKit
import AVFoundation

class ScanViewController: UIViewController {
    @IBOutlet weak var bottomLabel: UILabel!
    
    var captureSession = AVCaptureSession()
    var videoPreviewLayer: AVCaptureVideoPreviewLayer?
    var qrCodeFrameView: UIView?
    
    private let supportedCodeTypes = [AVMetadataObject.ObjectType.qr]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.customLayout()
        
        let deviceDiscoverySession = AVCaptureDevice.DiscoverySession(deviceTypes: [.builtInDualCamera], mediaType: AVMediaType.video, position: .back)
        
        guard let captureDevice = deviceDiscoverySession.devices.first else {
            print("Failed to get the camera device")
            return
        }
        
        do {
            // Get an instance of the AVCaptureDeviceInput class using the previous device object.
            let input = try AVCaptureDeviceInput(device: captureDevice)
            
            // Set the input device on the capture session.
            captureSession.addInput(input)
            
            // Initialize a AVCaptureMetadataOutput object and set it as the output device to the capture session.
            let captureMetadataOutput = AVCaptureMetadataOutput()
            captureSession.addOutput(captureMetadataOutput)
            
            // Set delegate and use the default dispatch queue to execute the call back
            captureMetadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            captureMetadataOutput.metadataObjectTypes = supportedCodeTypes
            //            captureMetadataOutput.metadataObjectTypes = [AVMetadataObject.ObjectType.qr]
            
        } catch {
            // If any error occurs, simply print it out and don't continue any more.
            print(error)
            return
        }
        
        // Initialize the video preview layer and add it as a sublayer to the viewPreview view's layer.
        videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        videoPreviewLayer?.videoGravity = AVLayerVideoGravity.resizeAspectFill
        videoPreviewLayer?.frame = view.layer.bounds
        view.layer.addSublayer(videoPreviewLayer!)
        
        // Start video capture.
        captureSession.startRunning()
        
        view.bringSubview(toFront: bottomLabel)
        
        
        // Initialize QR Code Frame to highlight the QR code
        qrCodeFrameView = UIView()
        
        if let qrCodeFrameView = qrCodeFrameView {
            qrCodeFrameView.layer.borderColor = UIColor.green.cgColor
            qrCodeFrameView.layer.borderWidth = 2
            view.addSubview(qrCodeFrameView)
            view.bringSubview(toFront: qrCodeFrameView)
        }
    }
    
    func customLayout() {
        self.navigationController?.isNavigationBarHidden = false
        self.removeNavBarLine()
        self.setNavigation(title: "Scan QRCode", withTintColor: .willianPinhoOrange, barTintColor: .willianPinhoBlue, andAttributes: [NSAttributedStringKey.font: UIFont.openWorkSansWithType(withSize: 24, type: WorkSansType.light), NSAttributedStringKey.foregroundColor: UIColor.willianPinhoOrange], prefersLargeTitles: false)
    }
    
    func launchApp(decodedURL: String) {
        
        let splitedUrl = splitUrl(url: decodedURL)
        let gistUrl = splitedUrl[0] + "//" + splitedUrl[1] +  splitedUrl[2] + "/"
        
        if decodedURL.hasPrefix(gistUrl) {
            let alert = Alert.showMessage(title: "Want to open this wonderful gist?", message: "Link of Gist: \(decodedURL)")
            let confirmAction = UIAlertAction(title: "Of Course!", style: UIAlertActionStyle.default, handler: { (action) -> Void in
                self.openViewControllerWithIdentifierWithGistId(storyBoard: "Main", identifier: "GistViewController", gistId: splitedUrl[4])
            })
            let cancelAction = UIAlertAction(title: "No, leave me alone!", style: UIAlertActionStyle.cancel, handler: nil)
            
            alert.addAction(confirmAction)
            alert.addAction(cancelAction)
            present(alert, animated: true, completion: nil)
        }
        else {
            let alert = Alert.showMessage(title: "Sorry!", message: "Maybe in the next version. \nThe developer didn't teach me how to open this kind of URL :(")
            alert.addAction(Alert.addOkAction(alert: alert))
            present(alert, animated: true, completion: nil)
            
        }
    }
    
    func splitUrl(url: String) ->  Array<String> {
        return url.components(separatedBy: "/")
    }
}

extension ScanViewController: AVCaptureMetadataOutputObjectsDelegate {
    
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        // Check if the metadataObjects array is not nil and it contains at least one object.
        if self.navigationController?.visibleViewController == self {
            if metadataObjects.count == 0 {
                qrCodeFrameView?.frame = CGRect.zero
                bottomLabel.text = "No QR Code detected"
                return
            }
            
            // Get the metadata object.
            let metadataObj = metadataObjects[0] as! AVMetadataMachineReadableCodeObject
            
            if supportedCodeTypes.contains(metadataObj.type) {
                // If the found metadata is equal to the QR code then update the status label's text and set the bounds
                let barCodeObject = videoPreviewLayer?.transformedMetadataObject(for: metadataObj)
                qrCodeFrameView?.frame = barCodeObject!.bounds
                
                if metadataObj.stringValue != nil {
                    launchApp(decodedURL: metadataObj.stringValue!)
                    bottomLabel.text = metadataObj.stringValue
                    
                }
            }
        }
        
        
        
    }
}
