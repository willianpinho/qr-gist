//
//  GistInformationPresenter.swift
//  QR-Gist
//
//  Created by Willian Pinho on 16/03/18.
//  Copyright © 2018 Willian Pinho. All rights reserved.
//

import Foundation
import UIKit

protocol GistInformationView: NSObjectProtocol {
    func setupCells(gist: Gist)
    func reloadTableView()
}
class GistInformationPresenter {
    var view: GistInformationView?
    var cells:[GistInformationTableViewCell] = []
    
    func setViewDelegate(view: GistInformationView) {
        self.view = view
    }
    
    func loadGistInformations(gist: Gist) {
        view?.setupCells(gist: gist)
        view?.reloadTableView()
    }
}
