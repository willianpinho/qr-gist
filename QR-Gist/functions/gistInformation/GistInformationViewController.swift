//
//  GistInformationViewController.swift
//  QR-Gist
//
//  Created by Willian Pinho on 16/03/18.
//  Copyright © 2018 Willian Pinho. All rights reserved.
//

import UIKit

class GistInformationViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    var presenter = GistInformationPresenter()
    var cells: [GistInformationTableViewCell] = []
    var gist: Gist?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setPresenterDelegate()
        self.setupTableView(tableView: self.tableView)
        self.customLayout()
        guard let gist = gist else {
            return print("Can't open gist")
        }
        presenter.loadGistInformations(gist: gist)
    }
    
    func setPresenterDelegate() {
        presenter.setViewDelegate(view: self)
    }
    
    func customLayout() {
        self.removeNavBarLine()
        self.setNavigation(title: "Gist Informations", withTintColor: .willianPinhoOrange, barTintColor: .willianPinhoBlue, andAttributes: [NSAttributedStringKey.font: UIFont.openWorkSansWithType(withSize: 24, type: WorkSansType.light), NSAttributedStringKey.foregroundColor: UIColor.willianPinhoOrange], prefersLargeTitles: false)
        
    }
}

extension GistInformationViewController: GistInformationView {
    func reloadTableView() {
        self.tableView.reloadData()
    }
    
    func setupCells(gist: Gist) {
        var currentCells:[GistInformationTableViewCell] = []
        let mirror = Mirror(reflecting: gist)
        for case let (name, value) in mirror.children {
            guard let name = name else {
                continue
            }
            
            
            if let gitvc = self.tableView.dequeueReusableCell(withIdentifier: "GistInformationTableViewCell") as? GistInformationTableViewCell {
                gitvc.titleLabel.text = name
                guard let stringValue = (value as AnyObject) as? String else {
                    continue
                }
                gitvc.valueLabel.text = stringValue
                currentCells.append(gitvc)
            }
        }
        
        cells = currentCells
    }
}

extension GistInformationViewController: UITableViewDelegate, UITableViewDataSource {
    func setupTableView(tableView: UITableView) {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.registerNibs([
            GistInformationTableViewCell.self
            ])
        tableView.estimatedRowHeight = UITableViewAutomaticDimension
        tableView.rowHeight = UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return cells[indexPath.row]
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cells.count
    }
}
