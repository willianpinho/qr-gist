//
//  UserService.swift
//  QR-Gist
//
//  Created by Willian Pinho on 17/03/18.
//  Copyright © 2018 Willian Pinho. All rights reserved.
//

import Foundation
import ObjectMapper
import SafariServices

class UserService {
    
    static func loginOAuthAuthorizer(completion: @escaping (_ success: Bool, _ message: String?, _ response: User?) -> Void) {
        let urlAuth = "https://github.com/login/oauth/authorize"
        guard let url =  URL(string: urlAuth) else {
            return print("Can't create url")
        }
        
        let request = NSMutableURLRequest(url: url)
        request.httpMethod = "GET"
        
        let task = URLSession.shared.dataTask(with: request as URLRequest) {
            data, response, error in
            if error != nil
            {
                print("error=\(String(describing: error))")
                return
            }
            
            let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            print("responseString = \(String(describing: responseString))")
            
            do {
                if let convertedJsonIntoDict = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                    let user = Mapper<User>().map(JSONObject: convertedJsonIntoDict)
                    return completion(true, nil, user)
                }
            } catch let error as NSError {
                return completion(false, error.localizedDescription, nil)
            }
        }
        task.resume()
    }
    
}


