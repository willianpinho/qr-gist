//
//  GistService.swift
//  QR-Gist
//
//  Created by Willian Pinho on 16/03/18.
//  Copyright © 2018 Willian Pinho. All rights reserved.
//

import Foundation
import ObjectMapper

class GistService {
    static func getGist(gistId: String, completion: @escaping (_ success: Bool, _ message: String?, _ response: Gist?) -> Void) {
        let gistUrl = "https://api.github.com/gists/"
        let urlWithParams = gistUrl + gistId
        guard let url =  URL(string: urlWithParams) else {
            return print("Can't create url")
        }
        
        let request = NSMutableURLRequest(url: url)
        request.httpMethod = "GET"
        
        let task = URLSession.shared.dataTask(with: request as URLRequest) {
            data, response, error in
            if error != nil
            {
                print("error=\(String(describing: error))")
                return
            }
            
            let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            print("responseString = \(String(describing: responseString))")
            
            do {
                if let convertedJsonIntoDict = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                    let gist = Mapper<Gist>().map(JSONObject: convertedJsonIntoDict)
                    return completion(true, nil, gist)
                }
            } catch let error as NSError {
                return completion(false, error.localizedDescription, nil)
            }
        }
        task.resume()
    }
    
    static func getCommentsFromGist(gist: Gist, completion: @escaping (_ success: Bool, _ message: String?, _ response: [Comment]?) -> Void) {
        let commentsUrl = "https://api.github.com/gists/\(gist.id!)/comments"
        guard let url =  URL(string: commentsUrl) else {
            return print("Can't create url")
        }
        
        let request = NSMutableURLRequest(url: url)
        request.httpMethod = "GET"
        
        let task = URLSession.shared.dataTask(with: request as URLRequest) {
            data, response, error in
            if error != nil
            {
                print("error=\(String(describing: error))")
                return
            }
            
            let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            print("responseString = \(String(describing: responseString))")
            
            do {
                if let convertedJsonIntoDict = try JSONSerialization.jsonObject(with: data!, options: []) as? Array<Any> {
                    let comments = Mapper<Comment>().mapArray(JSONObject: convertedJsonIntoDict)
                    return completion(true, nil, comments)
                }
            } catch let error as NSError {
                return completion(false, error.localizedDescription, nil)
            }
        }
        task.resume()
    }
}
